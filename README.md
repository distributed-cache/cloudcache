CloudCache is a distributed Memcached client meant to be deployed in AWS. Completed as part of a group project for a graduate level Cloud Computing course.
Has been successfully deployed and tested in AWS. 

Implemented Features:
1) Data duplication
2) Auto recovery of down instances (can fully recover data from up to 1 down memcached instance)

Features in progress:
1) Auto-scaling


**Steps to run:**

1) Ensure system has JAVA JDK and Maven installed
2) `mvn clean install`
3) `mvn exec:java`


[AWS Screenshot](https://gitlab.com/distributed-cache/cloudcache/wikis/AWS-Deployment)
