package com.uno.cloudcache;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.*;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.*;


public class AwsInstanceManager {

    private AmazonEC2 ec2Client;

    private List<Instance> awsInstances;

    public AwsInstanceManager() {

        awsInstances = new ArrayList<>();

        this.ec2Client = AmazonEC2ClientBuilder.standard().withRegion(Regions.US_EAST_2).build();
    }

    // Method to get public IPs of running AWS instances
    public List<Instance> getAwsInstances(ArrayList<String> awsImageIds) {

        DescribeInstancesRequest request = new DescribeInstancesRequest();
        request.setInstanceIds(awsImageIds);

        DescribeInstancesResult result = this.ec2Client.describeInstances(request);

        List<Reservation> reservations = result.getReservations();

        for (Reservation reservation : reservations) {
            awsInstances.add(reservation.getInstances().get(0));
            System.out.println();
        }
        return awsInstances;
    }

    // Method to start AWS instances currently in stopped state, instances must
    // already be created in AWS
    public void startAWSInstances(ArrayList<String> awsImageIds) {

        for (String instanceId : awsImageIds) {
            StartInstancesRequest startInstancesRequest = new StartInstancesRequest().withInstanceIds(instanceId);
            StartInstancesResult result = this.ec2Client.startInstances(startInstancesRequest);
            System.out.println(result.toString());
        }
        System.out.println("AWS instances starting..........");
        try {
            Thread.sleep(50000);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }

    public void stopAWSInstances(ArrayList<String> awsImageIds) {

        for (String instanceId : awsImageIds) {
            StopInstancesRequest stopInstancesRequest = new StopInstancesRequest().withInstanceIds(instanceId);
            StopInstancesResult result = this.ec2Client.stopInstances(stopInstancesRequest);
            System.out.println(result.toString());
        }
        System.out.println("AWS instances stopping..........");
        try {
            Thread.sleep(50000);
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }


    public void restartAWSInstance(ServerInstance serverInstance) {
        StartInstancesRequest startInstancesRequest = new StartInstancesRequest().withInstanceIds(serverInstance.getAwsInstanceId());
        StartInstancesResult result = this.ec2Client.startInstances(startInstancesRequest);
        List<InstanceStateChange> startingInstance = result.getStartingInstances();
        ArrayList<String> startingInstanceIds = new ArrayList<>();

        System.out.println("Restarting AWS instance : " + serverInstance.getAwsInstanceId());
        try {
            Thread.sleep(50000);
            for (InstanceStateChange instanceStateChange : startingInstance) {
                startingInstanceIds.add(instanceStateChange.getInstanceId());
            }

            List<Instance> startedServers = getAwsInstances(startingInstanceIds);
            for (Instance instance : startedServers) {
                try {
                    String host = instance.getPublicIpAddress();
                    Socket socket = new Socket(host, 11211);
                    PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    serverInstance.setHost(host);
                    serverInstance.setSocket(socket);
                    serverInstance.setPrintWriter(printWriter);
                    serverInstance.setBufferedReader(bufferedReader);
                }catch(IOException ioe){
                    ioe.printStackTrace();
                }

            }

        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }
    }
}