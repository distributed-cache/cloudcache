package com.uno.cloudcache;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ClientConnectionListener implements Runnable {

    ServerSocket serverSocket;
    Socket clientSocket;
    int portNumber = 4321;


    public ClientConnectionListener() {
        try {
            serverSocket = new ServerSocket(portNumber);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println("Waiting to accept client connection request...\n");
                clientSocket = serverSocket.accept();
                if (clientSocket.isConnected()) {
                    System.out.println("Successfully accepted connection request from client:" + clientSocket.getInetAddress() + "\n");
                }

                CloudCacheClientThread clientThread = new CloudCacheClientThread(clientSocket);
                clientThread.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
