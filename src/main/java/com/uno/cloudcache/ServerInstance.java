package com.uno.cloudcache;


import java.net.*;
import java.io.*;
import java.util.Scanner;

public class ServerInstance {

    private int id;
    private String awsInstanceId;
    private BufferedReader bufferedReader;
    private Scanner scanner;
    private PrintWriter printWriter;
    private Socket socket;
    private String host;
    private int port;


    public ServerInstance(int id, String serverId, String host, int port) {
        this.host = host;
        this.port = port;
        this.id = id;
        this.awsInstanceId = serverId;
        boolean connection_successful = false;
        try {
            socket = new Socket(host, port);
            printWriter = new PrintWriter(socket.getOutputStream(), true);
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            scanner = new Scanner(new InputStreamReader(socket.getInputStream()));
            connection_successful = true;
        } catch (IOException ioe) {
            ioe.printStackTrace();
            connection_successful = false;
            System.out.println("Connection status to the ip:" + host + " returned:" + connection_successful);
        } finally {
            if (!connection_successful) {
                try {
                    Thread.sleep(10000);
                    socket = new Socket(host, port);
                    printWriter = new PrintWriter(socket.getOutputStream(), true);
                    bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    scanner = new Scanner(new InputStreamReader(socket.getInputStream()));
                    connection_successful = true;
                    System.out.println("Finally retying connection status to the ip:" + host + " returned:" + connection_successful);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAwsInstanceId() {
        return awsInstanceId;
    }

    public void setAwsInstanceId(String awsInstanceId) {
        this.awsInstanceId = awsInstanceId;
    }

    public BufferedReader getBufferedReader() {
        return bufferedReader;
    }

    public Scanner getScanner() {
        return scanner;
    }

    public void setBufferedReader(BufferedReader bufferedReader) {
        this.bufferedReader = bufferedReader;
    }

    public PrintWriter getPrintWriter() {
        return printWriter;
    }

    public void setPrintWriter(PrintWriter printWriter) {
        this.printWriter = printWriter;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }


}
