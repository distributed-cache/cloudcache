package com.uno.cloudcache;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.*;

import com.amazonaws.services.ec2.model.Instance;


public class ElasticityHandler implements Runnable {
    @Override
    public void run() {
        System.out.println("Starting elasticity handler...\n");
        Scanner readCommand = new Scanner(System.in);
        String command = "";
        try {
            while (true) {

                if (readCommand.hasNextLine()) {
                    command = readCommand.nextLine();
                }

                if (command.equalsIgnoreCase("E")) {
                    //get all the <key,value> pairs from all the servers that is running now
                    HashMap allKeyValue = getAllKeyValuePairs();

                    // start our new server(s)
                    startNewExtendedInstance();

                    App.getCloudCacheInstance().resetConsistentHashInstance();

                    // clear the cache of all the memcached instances
                    flushAllInstances();

                    //rehash all the keys
                    reHashAllKeyValuePairs(allKeyValue);

                } else if (command.equalsIgnoreCase("S")) {

                    // disable scheduler so that auto recovery does not start
                    App.getCloudCacheInstance().disableScheduler();
                    //get all the <key,value> pairs from all the servers
                    HashMap allKeyValue = getAllKeyValuePairs();

                    // stop our new server(s)
                    stopNewExtendedInstance();

                    App.getCloudCacheInstance().resetConsistentHashInstance();

                    // clear the cache of all the memcached instances
                    flushAllInstances();

                    //rehash all the keys
                    reHashAllKeyValuePairs(allKeyValue);

                    App.getCloudCacheInstance().enableScheduler();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void flushAllInstances() {

        String command = "flush_all";
        String response = "";

        try {
            for (ServerInstance serverInstance : App.getCloudCacheInstance().getServerInstances()) {
                serverInstance.getPrintWriter().println(command);
                while ((response = serverInstance.getBufferedReader().readLine()) != null) {
                    if (response.equalsIgnoreCase("OK")) {
                        break;
                    } else {
                        continue;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startNewExtendedInstance() {
        AwsInstanceManager awsInstanceManager = new AwsInstanceManager();
        String[] elasticImageIds = App.getCloudCacheInstance().getElasticAwsImageIds();
        awsInstanceManager.startAWSInstances(new ArrayList<String>(Arrays.asList(elasticImageIds)));
        List<Instance> instances = awsInstanceManager.getAwsInstances(new ArrayList<String>(Arrays.asList(elasticImageIds)));
        App.getCloudCacheInstance().addElasticInstances(instances);
    }

    private void stopNewExtendedInstance() {
        AwsInstanceManager awsInstanceManager = new AwsInstanceManager();
        String[] elasticImageIds = App.getCloudCacheInstance().getElasticAwsImageIds();
        awsInstanceManager.stopAWSInstances(new ArrayList<String>(Arrays.asList(elasticImageIds)));
        List<Instance> instances = awsInstanceManager.getAwsInstances(new ArrayList<String>(Arrays.asList(elasticImageIds)));
        App.getCloudCacheInstance().removeElasticInstances(instances);
    }

    private HashMap<String, String> getAllKeyValuePairs() {
        HashMap<String, String> allKeyValuePairs = new HashMap<>();

        for (ServerInstance serverInstance : App.getCloudCacheInstance().getServerInstances()) {
            List<String> keys = KeyRecoveryHelper.getKeyDump(serverInstance.getHost());
            allKeyValuePairs.putAll(KeyRecoveryHelper.get(serverInstance.getId(), keys));
        }
        return allKeyValuePairs;
    }

    private void reHashAllKeyValuePairs(HashMap<String, String> allKeyValuePairs) {

        Integer expiration_time = 3600;
        Integer flag = 0;
        int server = -1;
        int totalRunningServers = App.getCloudCacheInstance().getMemcachedRunningInstancesCount();

        try {

            for (String key : allKeyValuePairs.keySet()) {

                server = Integer.parseInt(App.getCloudCacheInstance().getConsistentHashInstance().get(key).toString());
                String value = allKeyValuePairs.get(key);
                Integer datalength = value.length();

                BufferedReader br1 = App.getCloudCacheInstance().getNumServerInstanceMap().get(server).getBufferedReader();
                BufferedReader br2 = App.getCloudCacheInstance().getNumServerInstanceMap().get((server + 1) % totalRunningServers).getBufferedReader();

                App.getCloudCacheInstance().getNumServerInstanceMap().get(server).getPrintWriter().println("set " + key + " " + flag + " " + expiration_time + " " + datalength + "\r\n" + value + "\r\n");
                App.getCloudCacheInstance().getNumServerInstanceMap().get((server + 1) % totalRunningServers).getPrintWriter().println("set " + key + " " + flag +
                        " " + expiration_time + " " + datalength + "\r\n" + value + "\r\n");

                String response1 = "";
                String response2 = "";
                int counter = 0;

                while ((response1 = br1.readLine()) != null) {
                    if (response1.equalsIgnoreCase("STORED")) {
                        counter++;
                        break;
                    } else {
                        continue;
                    }
                }

                while ((response2 = br2.readLine()) != null) {
                    if (response2.equalsIgnoreCase("STORED")) {
                        counter++;
                        break;
                    } else {
                        continue;
                    }
                }

                if (counter != 2) {
                    System.out.println("Error, could not set key..." + key);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
