package com.uno.cloudcache;

public class App {

  private static CloudCacheManager ccm;

  public static void main(String[] args) {

    CloudCacheManager manager = getCloudCacheInstance();
    manager.go();

  }

  public static CloudCacheManager getCloudCacheInstance() {
    if (ccm == null) {
      ccm = new CloudCacheManager();
    }

    return ccm;

  }

}