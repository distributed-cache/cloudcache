package com.uno.cloudcache;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class KeyRecoveryHelper {

    public static List<String> getKeyDump(String host) {
        String user = "ubuntu";
        String password = "XXXXX";
        String command1 = "memcdump --servers=localhost";
        List<String> allKeys = new ArrayList<>();

        try {

            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            JSch jsch = new JSch();
            Session session = jsch.getSession(user, host, 22);
            session.setPassword(password);
            session.setConfig(config);
            session.connect();
            System.out.println("Connected");

            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command1);
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(System.err);

            InputStream in = channel.getInputStream();
            channel.connect();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
            String line = "";


            while (true) {

                while ((line = bufferedReader.readLine()) != null) {
                    System.out.println(line);
                    allKeys.add(line);
                }
                if (channel.isClosed()) {
                    System.out.println("exit-status: " + channel.getExitStatus());
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                }
            }
            channel.disconnect();
            session.disconnect();
            System.out.println("DONE");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return allKeys;
    }

    public static HashMap<String, String> get(int id, List<String> keys) {

        HashMap<String, String> keyValuePair = new HashMap<>();

        BufferedReader br = App.getCloudCacheInstance().getNumServerInstanceMap().get(id).getBufferedReader();
        PrintWriter pr = App.getCloudCacheInstance().getNumServerInstanceMap().get(id).getPrintWriter();

        try {

            for (String key : keys) {
                pr.println("get " + key + "\n");
                String line = "";

                while ((line = br.readLine()) != null) {
                    if (line.contains("VALUE ") || line.contains("END") || line.contains("ERROR")) {
                        continue;
                    } else {
                        keyValuePair.put(key, line);
                        break;
                    }
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return keyValuePair;
    }

}
