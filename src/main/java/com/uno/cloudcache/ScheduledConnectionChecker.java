package com.uno.cloudcache;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.TimerTask;

public class ScheduledConnectionChecker extends TimerTask {

    List<ServerInstance> serverInstances;
    int timeout;
    HashMap<Integer, Boolean> AliveList;

    public ScheduledConnectionChecker(List<ServerInstance> serverInstances) {
        this.serverInstances = serverInstances;
        this.timeout = 5000;
        AliveList = new HashMap<>();
        System.out.println("Starting scheduler for recovery manager...\n");
        for (ServerInstance instance : serverInstances) {
            AliveList.put(instance.getId(), true);
        }
    }

    @Override
    public void run() {

        if (App.getCloudCacheInstance().isSchedulerEnabled()) {
            for (ServerInstance serverInstance : serverInstances) {
                try {
                    InetAddress address = InetAddress.getByName(serverInstance.getHost());
                    if (!address.isReachable(timeout)) {
                        AliveList.put(serverInstance.getId(), false);
                        new RecoveryManager(serverInstance).start();
                    } else {
                        AliveList.put(serverInstance.getId(), true);
                    }
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
