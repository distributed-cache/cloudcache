package com.uno.cloudcache;

import java.net.*;
import java.io.*;
import java.util.*;

public class CloudCacheClientThread extends Thread {

    private Socket clientSocket;
    private PrintWriter writer;
    private BufferedReader reader;

    // Regex for set and get
    private final String SET = "SET\\s+[a-zA-Z0-9]+\\s+[0-9]+\\s+[0-9]+\\s+[0-9]+";
    private final String GET = "GET\\s+[a-zA-Z0-9]+";

    private HashMap<String, ServerInstance> idServerInstanceMap;

    public CloudCacheClientThread(Socket clientSocket) {
        this.clientSocket = clientSocket;
        try {
            this.writer = new PrintWriter(clientSocket.getOutputStream(), true);
            this.reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void run() {

        String line;
        while (true) {
            try {
                if (reader != null) {
                    line = reader.readLine();
                    processCommand(line);
                } else {
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    public void processCommand(String command) {
        if (command.equalsIgnoreCase("QUIT") || command == null) {
            try {
                System.out.println("Client " + clientSocket.getInetAddress() + " closed connection\n");
                clientSocket.close();
                reader.close();
                writer.close();
                clientSocket = null;
                reader = null;
                writer = null;
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        } else if (command.matches(SET)) {
            String value = "";
            try {
                value = reader.readLine();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
            String[] commandSplit = command.split("\\s+");
            String key = commandSplit[1];
            Integer flag = Integer.parseInt(commandSplit[2]);
            Integer expirationTime = Integer.parseInt(commandSplit[3]);
            Integer dataLength = Integer.parseInt(commandSplit[4]);
            String response = set(key, flag, expirationTime, dataLength, value);
            writer.println(response);
        } else if (command.matches(GET)) {
            String[] commandSplit = command.split("\\s+");
            String key = commandSplit[1];
            String response = get(key);
            writer.println(response);
        } else {
            try {
                clientSocket.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

    }

    public String set(String key, Integer flag, Integer expirationTime, Integer dataLength, String value) {
        int server = Integer.parseInt(App.getCloudCacheInstance().getConsistentHashInstance().get(key));

        HashMap<Integer, ServerInstance> numServerInstanceMap = App.getCloudCacheInstance().getNumServerInstanceMap();
        int memcachedInstancesCount = App.getCloudCacheInstance().getMemcachedRunningInstancesCount();
        String response = "";
        try {

            BufferedReader br1 = numServerInstanceMap.get(server).getBufferedReader();
            BufferedReader br2 = numServerInstanceMap.get((server + 1) % memcachedInstancesCount).getBufferedReader();

            numServerInstanceMap.get(server).getPrintWriter().println("set " + key + " " + flag + " " + expirationTime + " " + dataLength + "\r\n" + value + "\r\n");

            numServerInstanceMap.get((server + 1) % memcachedInstancesCount).getPrintWriter().println("set " + key + " " + flag + " " + expirationTime + " " + dataLength + "\r\n" + value + "\r\n");

            String response1 = "";
            String response2 = "";

            int counter = 0;

            while ((response1 = br1.readLine()) != null) {
                if (response1.equalsIgnoreCase("STORED")) {
                    counter++;
                    break;
                } else {
                    continue;
                }
            }

            while ((response2 = br2.readLine()) != null) {
                if (response2.equalsIgnoreCase("STORED")) {
                    counter++;
                    break;
                } else {
                    continue;
                }
            }

            if (counter == 2) {
                response = "STORED";
            } else {
                response = "Error, could not set key: " + key;
            }
        } catch (IOException ioe) {
            response = "Internal error";
            ioe.printStackTrace();
        } catch (Exception e) {
            response = "Internal error";
            e.printStackTrace();
        }

        return response;
    }

    public String get(String key) {

        int server = Integer.parseInt(App.getCloudCacheInstance().getConsistentHashInstance().get(key));
        int duplicationServer = App.getCloudCacheInstance().getNextServerId(server);

        String line = "";
        BufferedReader br1, br2;
        //Scanner sc1, sc2;
        boolean keyfound = false;

        HashMap<Integer, ServerInstance> numServerInstanceMap = App.getCloudCacheInstance().getNumServerInstanceMap();

        try {
            numServerInstanceMap.get(server).getPrintWriter().println("get " + key + "\n");

            br1 = numServerInstanceMap.get(server).getBufferedReader();
            //sc1 = numServerInstanceMap.get(server).getScanner();


            while ((line = br1.readLine()) != null) {

                if (line.contains("VALUE ") || line.contains("END") || line.contains("ERROR")) {
                    keyfound = false;
                    continue;
                } else {
                    keyfound = true;
                    break;
                }
            }
            if (!keyfound) {

                numServerInstanceMap.get(duplicationServer).getPrintWriter().println("get " + key + "\n");
                br2 = numServerInstanceMap.get(duplicationServer).getBufferedReader();
                //sc2 = numServerInstanceMap.get(duplicationServer).getScanner();

                while ((line = br2.readLine()) != null) {

                    if (line.contains("VALUE ") || line.contains("END") || line.contains("ERROR")) {
                        keyfound = false;
                        continue;
                    } else {
                        keyfound = true;
                        break;
                    }
                }
            }

            if (keyfound) {
                return line;
            } else {
                line = "Key not found";
                return line;
            }
        } catch (Exception e) {
            e.printStackTrace();
            line = "Internal error";
        }

        return line;
    }

}