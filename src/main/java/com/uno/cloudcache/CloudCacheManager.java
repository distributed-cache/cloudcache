package com.uno.cloudcache;

import java.util.*;
import java.net.*;
import java.io.*;

import com.amazonaws.services.ec2.model.Instance;

public class CloudCacheManager {

    int memcachedInstancesCount = 5;

    boolean schedulerEnabled = false;

    // client connection vars
    private final int PORT = 11211;

    // AWS vars
    AwsInstanceManager awsInstanceManager;
    private final String[] awsImageIds = new String[]{"i-00b70ae85b99d7e75", "i-07fed1c3e46992a70",
            "i-09717af477adaaa24", "i-0aba54f2ada075017", "i-0f9e31c1a5f18cbaf"};

    private final String[] elasticAwsImageIds = new String[]{"i-00d67162d0153be75", "i-0c43a56fafb0c78f9"};

    private List<Instance> awsInstances;

    // server instances
    private List<ServerInstance> serverInstances;
    private HashMap<Integer, ServerInstance> numServerInstanceMap;


    // connection checker
    private ScheduledConnectionChecker scheduledConnectionChecker;

    // md5 and consistent hash declaration
    private ArrayList<String> nodes = new ArrayList<>();
    private MD5 md5 = new MD5();
    private ConsistentHash<String> consistentHash = null;

    // testing
    ArrayList<String> dataList = new ArrayList<>();
    ArrayList<Long> setTimes = new ArrayList<>();
    ArrayList<Long> getTimes = new ArrayList<>();

    private ServerSocket serverSocket;
    private Socket clientSocket;

    // Regex for set and get
    private final String SET = "SET ^[a-zA-Z0-9]+$ ^\\d+$ ^\\d+$ ^\\d+$";
    private final String GET = "GET ^[a-zA-Z0-9]+$";

    public ServerInstance ServerInstancefromId(int serverId) {
        return numServerInstanceMap.get(serverId);
    }

    public boolean isSchedulerEnabled() {
        return schedulerEnabled;
    }

    public void enableScheduler() {
        this.schedulerEnabled = true;
    }


    public void disableScheduler() {
        this.schedulerEnabled = false;
    }

    public void go() {

        // start aws instances and get publicgit IPs
        awsSetup();

        // create list of serverinstances
        this.serverInstances = getServerInstances();

        // assign internal id/num to instances
        assignNum();

        addNodes();

        getConsistentHashInstance();

        // start heartbeat checker
        startScheduledConnectionChecker();

        Thread elasticityThread = new Thread(new ElasticityHandler());
        elasticityThread.start();

        // get connection from client
        //getClientConnection();


        Thread clientListner = new Thread(new ClientConnectionListener());
        clientListner.start();


        try {
            elasticityThread.join();
            clientListner.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void addElasticInstances(List<Instance> elasticInstances) {

        int id = serverInstances.size();

        for (Instance elasticInstance : elasticInstances) {
            ServerInstance serverInstance = new ServerInstance(id, elasticInstance.getInstanceId(), elasticInstance.getPublicIpAddress(), PORT);
            serverInstances.add(serverInstance);
            numServerInstanceMap.put(id, serverInstance);
            id++;
        }
        addNodes();
    }

    public void removeElasticInstances(List<Instance> elasticInstances) {

        List<ServerInstance> removeList = new ArrayList<>();

        for (Instance elasticInstance : elasticInstances) {
            //ServerInstance serverInstance = new ServerInstance(id, elasticInstance.getInstanceId(), elasticInstance.getPublicIpAddress(), PORT);
            for (ServerInstance serverInstance : serverInstances) {
                if (elasticInstance.getInstanceId().equals(serverInstance.getAwsInstanceId())) {
                    removeList.add(serverInstance);
                    break;
                }
            }
        }
        for (ServerInstance removeInstance : removeList) {
            numServerInstanceMap.remove(removeInstance.getId());
            serverInstances.remove(removeInstance);
        }
        addNodes();
    }

    public String[] getElasticAwsImageIds() {
        return elasticAwsImageIds;
    }

    public ConsistentHash<String> getConsistentHashInstance() {
        if (consistentHash == null) {
            consistentHash = new ConsistentHash<>(md5, 10, nodes);
        }
        return consistentHash;
    }

    public void resetConsistentHashInstance() {
        consistentHash = null;
    }

    private void assignNum() {
        numServerInstanceMap = new HashMap<>();
        for (ServerInstance instance : serverInstances) {
            numServerInstanceMap.put(instance.getId(), instance);
            System.out.println("Server num: "+instance.getId()+" Instance id: " +instance.getAwsInstanceId()+" Ip: "+instance.getHost());
        }
    }

    private void startScheduledConnectionChecker() {
        long interval = 30000;
        Timer timer = new Timer();
        this.scheduledConnectionChecker = new ScheduledConnectionChecker(serverInstances);
        App.getCloudCacheInstance().enableScheduler();
        timer.scheduleAtFixedRate(scheduledConnectionChecker, interval, interval);
    }

    // start aws instances and get public IPs
    private void awsSetup() {
        awsInstanceManager = new AwsInstanceManager();
        startAwsInstances();
        awsInstances = getAwsInstances();
    }

    private void startAwsInstances() {
        awsInstanceManager.startAWSInstances(new ArrayList<String>(Arrays.asList(awsImageIds)));
    }

    private List<Instance> getAwsInstances() {
        return awsInstanceManager.getAwsInstances(new ArrayList<String>(Arrays.asList(awsImageIds)));
    }

    public List<ServerInstance> getServerInstances() {
        List<ServerInstance> serverInstances = new ArrayList<>();
        int id = 0;
        for (Instance i : awsInstances) {
            ServerInstance serverInstance = new ServerInstance(id, i.getInstanceId(), i.getPublicIpAddress(), PORT);
            serverInstances.add(serverInstance);
            id++;
        }
        return serverInstances;
    }

    private void addNodes() {

        nodes.clear();

        for (Integer instanceNum : numServerInstanceMap.keySet()) {
            nodes.add(instanceNum.toString());
        }
    }

    //client app connects to CloudCache - if third party app was connecting to this service
    private void getClientConnection() {
        try {
            serverSocket = new ServerSocket(4321);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                clientSocket = serverSocket.accept();
                CloudCacheClientThread clientThread = new CloudCacheClientThread(clientSocket);
                clientThread.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //***********************************Memcached Api Methods***************************************

    public int getMemcachedRunningInstancesCount() {
        return numServerInstanceMap.size();
    }

    public HashMap<Integer, ServerInstance> getNumServerInstanceMap() {
        return numServerInstanceMap;
    }

    public void set(String key, String value) {
        int server = Integer.parseInt(getConsistentHashInstance().get(key).toString());
        System.out.println("Inside Set Server:" + server + " key:" + key);

        try {

            Integer expiration_time = 3600;
            Integer flag = 0;
            Integer datalength = value.length();

            long start = System.nanoTime();

            BufferedReader br1 = numServerInstanceMap.get(server).getBufferedReader();
            BufferedReader br2 = numServerInstanceMap.get((server + 1) % memcachedInstancesCount).getBufferedReader();

            numServerInstanceMap.get(server).getPrintWriter().println("set " + key + " " + flag + " " + expiration_time + " " + datalength + "\r\n" + value + "\r\n");
            //String line1 = numServerInstanceMap.get(server).getBufferedReader().readLine();
            //String line3 = numServerInstanceMap.get(server).getBufferedReader().readLine();

            numServerInstanceMap.get((server + 1) % memcachedInstancesCount).getPrintWriter().println("set " + key + " " + flag +
                    " " + expiration_time + " " + datalength + "\r\n" + value + "\r\n");

            //String line2 = numServerInstanceMap.get((server + 1) % memcachedInstancesCount).getBufferedReader().readLine();
            //String line4 = numServerInstanceMap.get((server + 1) % memcachedInstancesCount).getBufferedReader().readLine();


            String response1 = "";
            String response2 = "";
            int counter = 0;

            while ((response1 = br1.readLine()) != null) {
                if (response1.equalsIgnoreCase("STORED")) {
                    counter++;
                    break;
                } else {
                    continue;
                }
            }

            while ((response2 = br2.readLine()) != null) {
                if (response2.equalsIgnoreCase("STORED")) {
                    counter++;
                    break;
                } else {
                    continue;
                }
            }

            long finish = System.nanoTime();

            if (counter == 2) {
                setTimes.add(finish - start);
                System.out.println("Set time: " + (finish - start) + " nano seconds");
            } else {
                System.out.println("Error, could not set key..." + key);
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getNextServerId(int serverId) {
        return (serverId + 1) % memcachedInstancesCount;

    }

    public int getPreviousServerId(int serverId) {
        return (serverId - 1 + memcachedInstancesCount) % memcachedInstancesCount;
    }


    public void get(String key, String expectedValue) {
        int server = Integer.parseInt(getConsistentHashInstance().get(key).toString());

        System.out.println("Inside Get Server:" + server + " key:" + key);
        try {
            long start = System.nanoTime();
            numServerInstanceMap.get(server).getPrintWriter().println("get " + key + "\n");
            String line = "";
            BufferedReader br = numServerInstanceMap.get(server).getBufferedReader();
            while ((line = br.readLine()) != null) {
                if (line.equals(expectedValue)) {
                    long finish = System.nanoTime();
                    getTimes.add(finish - start);
                    System.out.println("Get time: " + (finish - start) + " nano seconds");
                    break;
                } else if (line.contains("VALUE ") || line.contains("END") || line.contains("ERROR")) {
                    continue;
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String get(String key) {
        int server = Integer.parseInt(getConsistentHashInstance().get(key).toString());
        int duplicationServer = getNextServerId(server);

        String line = "";
        BufferedReader br1, br2;
        boolean keyfound = false;

        //System.out.println("Inside Get Server:" + server + " key:" + key);
        try {
            //long start = System.nanoTime();
            numServerInstanceMap.get(server).getPrintWriter().println("get " + key + "\n");
            numServerInstanceMap.get(duplicationServer).getPrintWriter().println("get " + key + "\n");


            br1 = numServerInstanceMap.get(server).getBufferedReader();
            br2 = numServerInstanceMap.get(duplicationServer).getBufferedReader();


            while ((line = br1.readLine()) != null) {
                if (line.contains("VALUE ") || line.contains("END") || line.contains("ERROR")) {
                    keyfound = false;
                    continue;
                } else {
                    keyfound = true;
                    break;
                }
            }
            if (!keyfound) {
                while ((line = br2.readLine()) != null) {
                    if (line.contains("VALUE ") || line.contains("END") || line.contains("ERROR")) {
                        keyfound = false;
                        continue;
                    } else {
                        keyfound = true;
                        break;
                    }
                }
            }

            if (keyfound) {
                return line;
            } else {
                line = "";
                return line;
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            line = "";
        } catch (Exception e) {
            e.printStackTrace();
            line = "";
        }

        return line;
    }

}