package com.uno.cloudcache;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class RecoveryManager extends Thread {
    ServerInstance serverInstance;

    public RecoveryManager(ServerInstance serverInstance) {

        this.serverInstance = serverInstance;
    }

    public void run() {
        AwsInstanceManager awsInstanceManager = new AwsInstanceManager();
        awsInstanceManager.restartAWSInstance(serverInstance);
        recoverInstance();

    }

    private void recoverInstance() {
        int failedServerId = serverInstance.getId();
        int nextServerId = App.getCloudCacheInstance().getNextServerId(failedServerId);
        int previousServerId = App.getCloudCacheInstance().getPreviousServerId(failedServerId);

        ServerInstance nextServer = App.getCloudCacheInstance().ServerInstancefromId(nextServerId);
        ServerInstance previousServer = App.getCloudCacheInstance().ServerInstancefromId(previousServerId);
        HashMap<String, String> keyValuePairs = getRecoveredKeyValuePairs(nextServer, previousServer);
        setAll(failedServerId, keyValuePairs);

    }

    private HashMap<String, String> getRecoveredKeyValuePairs(ServerInstance nextServer, ServerInstance previousServer) {
        String nextHost = nextServer.getHost();
        String previousHost = previousServer.getHost();
        List<String> keysFromNextServer = KeyRecoveryHelper.getKeyDump(nextHost);
        List<String> keysFromPreviousServer = KeyRecoveryHelper.getKeyDump(previousHost);

        List<String> previousKeys = processPreviousKeys(keysFromPreviousServer, previousServer.getId());
        List<String> nextKeys = processNextKeys(keysFromNextServer, nextServer.getId());
        HashMap<String, String> previousKeyValueMap = KeyRecoveryHelper.get(previousServer.getId(), previousKeys);
        HashMap<String, String> nextKeyValueMap = KeyRecoveryHelper.get(nextServer.getId(), nextKeys);

        HashMap<String, String> allRecoveredKeyValueMap = new HashMap<>();
        allRecoveredKeyValueMap.putAll(previousKeyValueMap);
        allRecoveredKeyValueMap.putAll(nextKeyValueMap);

        return allRecoveredKeyValueMap;
    }

    private List<String> processPreviousKeys(List<String> keysFromPreviousServer, int prevServerId) {

        List<String> previousKeys = new ArrayList<>();
        CloudCacheManager cloudCacheManager = App.getCloudCacheInstance();
        ConsistentHash ch = cloudCacheManager.getConsistentHashInstance();

        for (String key : keysFromPreviousServer) {
            int serverId = Integer.parseInt(ch.get(key).toString());
            if (serverId == prevServerId) {
                previousKeys.add(key);
            }
        }
        return previousKeys;
    }

    private List<String> processNextKeys(List<String> keysFromNextServer, int nextServerId) {
        List<String> nextKeys = new ArrayList<>();
        CloudCacheManager cloudCacheManager = App.getCloudCacheInstance();
        ConsistentHash ch = cloudCacheManager.getConsistentHashInstance();

        for (String key : keysFromNextServer) {
            int serverId = Integer.parseInt(ch.get(key).toString());
            if (serverId != nextServerId) {
                nextKeys.add(key);
            }
        }
        return nextKeys;
    }

    public void setAll(int serverInstanceId, HashMap<String, String> keyValuePairs) {

        BufferedReader br = App.getCloudCacheInstance().getNumServerInstanceMap().get(serverInstanceId).getBufferedReader();
        PrintWriter pr = App.getCloudCacheInstance().getNumServerInstanceMap().get(serverInstanceId).getPrintWriter();

        try {

            Integer expiration_time = 3600;
            Integer flag = 0;
            Integer datalength = 0;

            for (String key : keyValuePairs.keySet()) {
                String value = keyValuePairs.get(key);
                datalength = value.length();
                pr.println("set " + key + " " + flag + " " + expiration_time + " " + datalength + "\r\n" + value + "\r\n");
                String response = "";

                while ((response = br.readLine()) != null) {
                    if (response.equalsIgnoreCase("STORED")) {
                        break;
                    } else {
                        continue;
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



}
