package com.uno.cloudcache;

import java.util.*;

public class ConsistentHash<T> {

    private final MD5 hashFunction;
    private final int numberOfReplicas;
    private final SortedMap<String, T> circle = new TreeMap<>();
    private List<Integer> replicaList;

    public ConsistentHash(MD5 hashFunction, int numberOfReplicas, Collection<T> nodes) {

        this.hashFunction = hashFunction;
        this.numberOfReplicas = numberOfReplicas;
        replicaList = new ArrayList(numberOfReplicas);

        for (T node : nodes) {
            add(node);
        }
    }

    public void add(T node) {
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < numberOfReplicas; i++) {
            int extension = random.nextInt(97);
            replicaList.add(extension);
            circle.put(hashFunction.hash(node.toString() + extension), node);
        }
    }

    public void remove(T node) {
        for (int i = 0; i < numberOfReplicas; i++) {
            circle.remove(hashFunction.hash(node.toString() + replicaList.remove(i)));
        }
    }

    public T get(Object key) {
        if (circle.isEmpty()) {
            return null;
        }
        String hash = hashFunction.hash(key.toString());
        if (!circle.containsKey(hash)) {
            SortedMap<String, T> tailMap = circle.tailMap(hash);
            hash = tailMap.isEmpty() ? circle.firstKey() : tailMap.firstKey();
        }
        return circle.get(hash);
    }

}